# Node version
FROM node:latest

# Dir app
WORKDIR /app

# File copy
ADD . /app

# Dependency
RUN npm install

# port info
EXPOSE 3000

# Command
CMD ["npm", "start"]

# This is a comment for create commit
