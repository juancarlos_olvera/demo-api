//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var requestJson = require('request-json');

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var cors = require('cors');
app.use(
  cors({
    credentials: true, 
    origin: true
  })
);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.send("HI");
});

app.get("/file/:name", function(req, res, next){
  var options = {
    root: __dirname + '/public/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };

  var fileName = req.params.name;
  res.sendFile(fileName, options, function (err) {
    if (err) {
      next(err);
    } else {
      console.log('Sent:', fileName);
    }
  });
});

app.post("/post", function(req, res){
  res.send({key1:"value 1",key2:"value2"});
});

app.delete("/delete", function(req, res){
  res.sendStatus(200); 
});

app.get("/movimientos", function(req, res){
  var client = requestJson.createClient('https://api.mlab.com/api/1/');
  client.get('databases/bdbanca3mb12238/collections/movimientos/?apiKey=_5Vr3AEJ7-HkOG3j02LyCE16d07S0eoY', function(errClient, resClient, bodyClient) {
    if(errClient){
      console.log(errClient);
    } else {
      res.send(bodyClient);
    }
  }); 
});

app.post("/movimientos", function(req, res){
  var data = req.body;
  var client = requestJson.createClient('https://api.mlab.com/api/1/');
  client.post('databases/bdbanca3mb12238/collections/movimientos/?apiKey=_5Vr3AEJ7-HkOG3j02LyCE16d07S0eoY', data, function(errClient, resClient, bodyClient) {
    if(errClient){
      console.log(errClient);
    } else {
      res.send(bodyClient);
    }
  }); 
});